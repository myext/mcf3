<?php
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
/**
* Param Filter: Virtuemart 3 search module
* Version: 3.0.8 (2020.07.05)
* Author: Dmitriy Usov
* Copyright: Copyright (C) 2012-2015 usovdm
* License GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
* http://myext.ru
**/
jimport('joomla.installer.helper');
$installer = JInstaller::getInstance();
// store original path to install directory
$installerBasePath = $installer->getPath('source');
$name = $installer->manifest->name->__toString();
$pkgs = array( 
	'param'=> array('Multiple Custom Filter plugin',JPATH_ROOT.DS.'plugins'.DS.'vmcustom'.DS)
);
$db = JFactory::getDBO ();
// Values table 
$query = "CREATE TABLE IF NOT EXISTS `#__virtuemart_product_custom_plg_param_ref` (";
$tablesFields = array(
	'id' => 'int(11) NOT NULL AUTO_INCREMENT',
	'virtuemart_product_id' => 'int(11) NOT NULL',
	'virtuemart_custom_id' => 'int(11) NOT NULL',
	'val' => 'int(11) NOT NULL',
	'intval' => 'double NOT NULL',
);
foreach ($tablesFields as $fieldname => $fieldtype) {
	$query .= '`' . $fieldname . '` ' . $fieldtype . " , ";
}
$query .= "	      PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='customvalues reference' AUTO_INCREMENT=1 ;";
$db->setQuery ($query);
if (!$db->query ()) {
	JError::raiseWarning (1, $name . '::onStoreInstallPluginTable: ' . JText::_ ('COM_VIRTUEMART_SQL_ERROR') . ' ' . $db->stderr (TRUE));
	echo $name . '::onStoreInstallPluginTable: ' . JText::_ ('COM_VIRTUEMART_SQL_ERROR') . ' ' . $db->stderr (TRUE);
}
// Reference table
$query = "CREATE TABLE IF NOT EXISTS `#__virtuemart_product_custom_plg_param_values` (";
$tablesFields = array(
	  'id' => 'int(11) NOT NULL AUTO_INCREMENT',
	  'virtuemart_custom_id' => 'int(11) NOT NULL',
	  'value' => 'varchar(255) NOT NULL',
	  'status' => 'int(1) NOT NULL',
	  'published' => 'int(1) NOT NULL',
	  'ordering' => 'int(5) NOT NULL'
);
foreach ($tablesFields as $fieldname => $fieldtype) {
	$query .= '`' . $fieldname . '` ' . $fieldtype . " , ";
}
$query .= "	      PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='customvalues' AUTO_INCREMENT=1 ;";
$db->setQuery ($query);
if (!$db->query ()) {
	JError::raiseWarning (1, $name . '::onStoreInstallPluginTable: ' . JText::_ ('COM_VIRTUEMART_SQL_ERROR') . ' ' . $db->stderr (TRUE));
	echo $name . '::onStoreInstallPluginTable: ' . JText::_ ('COM_VIRTUEMART_SQL_ERROR') . ' ' . $db->stderr (TRUE);
}
foreach( $pkgs as $pkg => $v ){
	$pkg_path = $v[1];
	$pkgname = $v[0];

	$subinstaller = new JInstaller();
	// $subinstaller->setPath('source', $installerBasePath.DS.$pkg);
	// $subinstaller->findManifest();

	if( $subinstaller->install( $installerBasePath.DS.$pkg ) ){
		$msgcolor = "#E0FFE0";
		$msgtext  = "$pkgname successfully installed.";
	}else{
		$msgcolor = "#FFD0D0";
		$msgtext  = "ERROR: Could not install the $pkgname. Please install manually.";
	}
	?>
	<table bgcolor="<?php echo $msgcolor; ?>" width ="100%">
	<tr style="height:30px">
		<td width="50px"><img src="/administrator/images/tick.png" height="20px" width="20px"></td>
		<td><font size="2"><b><?php echo $msgtext; ?></b></font></td>
	</tr>
	</table>
	<?php
}