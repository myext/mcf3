<?php
defined('_JEXEC') or die('Restricted access');
/**
* Param Filter: Virtuemart 3 search module
* Version: 3.0.8 (2020.07.05)
* Author: Dmitriy Usov
* Copyright: Copyright (C) 2012-2015 usovdm
* License GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
* http://myext.ru
**/

$html .= '<div class="filter_category">';
if(!empty($mcf_category_heading))
	$html .= '<div class="heading">'.$mcf_category_heading.'</div>';
$html .= '<ul class="values" data-id="c">'.recursiveList($categories,$cids,$parent_category_id,0,'checkbox').'</ul>';
$html .= '</div>';